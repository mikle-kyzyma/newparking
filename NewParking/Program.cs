﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParking
{         
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            Parking parking = Parking.getInstance();

            while (true)
            {
                ShowMenu();
                Console.Write(": Choose number of menu: ");
                try
                {
                    i = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                    switch (i)
                    {
                        case 1:
                            Setting.ShowBalanceParking();
                            break;
                        case 2:
                            Setting.ShowAmountMoneyEarnd();
                            break;
                        case 3:
                            Setting.ShowNumberBusyFreePlaces();
                            break;
                        case 4:
                            Setting.ShowListAllCars();
                            break;
                        case 5:
                            Setting.ShowListAllTransactions();
                            break;
                        case 6:
                            Setting.ReadFromoFileTransactions();
                            break;
                        case 7:
                            parking.AddCarOnParking();
                            break;
                        case 8:
                            parking.RemoveCarFromParking();
                            break;
                        case 9:
                            parking.RefillBalance();
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                
                if (i == 0) break;
                Console.WriteLine("Press any key to Cotinue");
                Console.ReadKey();
            }

            Console.WriteLine("Press any key to Exit");
            Console.ReadKey();
        }
        static void ShowMenu()
        {
            Console.WriteLine();
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("            Menu Parkig                          ");
            Console.WriteLine("1. Current balance of Parking                    ");
            Console.WriteLine("2. The amount of money earned in the last minute ");
            Console.WriteLine("3. Number free/busy places on the Parking        ");
            Console.WriteLine("4. List all of vehicles                          ");
            Console.WriteLine("5. Display all transactions for the last minute  ");
            Console.WriteLine("6. Display all transactions history from file    ");
            Console.WriteLine("7. Put a Vehicle on Parking                      ");
            Console.WriteLine("8. Pick up the vehicle from the parking          ");
            Console.WriteLine("9. Refill the balance of a particular Vehicleу   ");
            Console.WriteLine("0. EXIT                                          ");
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("* Type of cars called: CAR, TRUCK, BUS, MOTORBIKE");
            Console.WriteLine();
        }
    }   
}
